from django.db import models
from django.contrib.auth.models import User

class Instituicao(models.Model):
	nome_da_instituicao = models.CharField('nome da instituição', max_length=200)
	sigla = models.CharField('sigla da instituição', max_length=20)
	cidade = models.CharField('cidade da instituição', max_length=200)
	nome_da_unidade = models.CharField('nome da unidade', max_length=200, blank=True)
	
	def __str__(self):
		return "%s/%s" % (self.sigla, self.nome_da_unidade)

class Curso(models.Model):
	nome_do_curso = models.CharField('nome do curso', max_length=200)
	instituicao = models.ForeignKey('Instituicao')

	def __str__(self):
		return "%s - %s" % (self.nome_do_curso, self.instituicao)

class Perfil(models.Model):

	class Meta:
		db_table = 'pesquisador'
		verbose_name = 'pesquisador'
		verbose_name_plural = 'pesquisadores'

	email = models.EmailField('e-mail do pesquisador', blank=True)
	
	usuario = models.ForeignKey(User)
	
	nome_do_pesquisador = models.CharField('nome do pesquisador', max_length=200, blank=True)
	nome_cientifico = models.CharField('nome de referência científica do pesquisador', max_length=200, blank=True)
	opcoes_sexo = (
		('M', 'Masculino'),
		('F', 'Feminino'),
	)
	sexo = models.CharField('sexo do pesquisador', max_length=1, choices=opcoes_sexo, blank=True)
	
	opcoes_titulo = (
		('graduado_bacharel', 'Graduado em Bacharelado'),
		('graduado_licenciatura', 'Graduado em Licenciatura'),
		('especialita', 'Especialista'),
		('mestre', 'Mestre'),
		('doutor', 'Doutor'),
		
	)
	titulo = models.CharField('título do pesquisador', max_length=100, choices=opcoes_titulo, blank=True)
	opcoes_tipo_pesquisador = (
		('estudante', 'Estudante'),
		('professor', 'Professor'),
		('tecnico', 'Técnico'),
	)
	tipo_pesquisador = models.CharField('tipo do pesquisador', max_length=15, choices=opcoes_tipo_pesquisador, blank=True)
	curso = models.ForeignKey(Curso, blank=True)
	url_lates = models.CharField('URL curriculo do LATES', max_length=200, blank=True)
	
	avatar = models.ImageField(upload_to="images/avatar/", blank=True)
	bio = models.TextField('Escreva uma breve biografia', blank=True)

	def __str__(self):
		return "{}".format(self.usuario)