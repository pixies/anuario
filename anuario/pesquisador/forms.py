from django import forms 
from .models import Perfil, Curso

class FormEditPerfil(forms.ModelForm):

		email = forms.EmailField(label='E-mail auternativo', required=False)

		nome_do_pesquisador = forms.CharField(label='nome do pesquisador', required=False)
		nome_cientifico = forms.CharField(label='nome de referência científica do pesquisador', required=False)
		
		opcoes_sexo = (
			('M', 'Masculino'),
			('F', 'Feminino'),
		)
		
		sexo = forms.CharField(label='sexo do pesquisador', widget=forms.Select(choices=opcoes_sexo), required=False)
		
		opcoes_titulo = (
			('graduado_bacharel', 'Graduado em Bacharelado'),
			('graduado_licenciatura', 'Graduado em Licenciatura'),
			('especialita', 'Especialista'),
			('mestre', 'Mestre'),
			('doutor', 'Doutor'),
			
		)
		titulo = forms.CharField(label='título do pesquisador', widget=forms.Select(choices=opcoes_titulo), required=False)
		
		opcoes_tipo_pesquisador = (
			('estudante', 'Estudante'),
			('professor', 'Professor'),
			('tecnico', 'Técnico'),
		)
		tipo_pesquisador = forms.CharField(label='sexo do pesquisador', widget=forms.Select(choices=opcoes_tipo_pesquisador), required=False)
		
		opcoes_curso = (forms.ModelChoiceField(queryset=Curso.objects.all()).choices)

		curso = forms.CharField(label='Qual seu curso?', widget=forms.Select(choices=opcoes_curso), required=False)
		url_lates = forms.CharField(label='URL curriculo do LATES', required=False)

		class Meta:
			model = Perfil
			fields = (
	    		'nome_do_pesquisador',
	    		'nome_cientifico',
	    		'email',
	    		'sexo',
	    		'titulo',
	    		'tipo_pesquisador',
	    		'curso',
	    		'url_lates',
	    		)

		widgets = {
            'nome_do_pesquisador': forms.TextInput(attrs={'placeholder': 'Seu nome completo'}),
            'nome_cientifico': forms.TextInput(attrs={'placeholder': 'Para referencias bibliográficas'}),
        }

