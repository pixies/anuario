from django.contrib import admin
from django.contrib import admin
from .models import Perfil, Curso, Instituicao

# Register your models here.
admin.site.register(Perfil)
admin.site.register(Curso)
admin.site.register(Instituicao)