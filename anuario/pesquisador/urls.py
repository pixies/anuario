from django.conf.urls import url

from .views import todos_pesquisadores, editar_pesquisador, meu_perfil, editar_meu_perfil #, pesquisador, edit_pesquisador

urlpatterns = [

    url(r'^todos/$', todos_pesquisadores, name="todos"),
    #url(r'(?P<id_pesquisador>\[0-9]+)/$', pesquisador, name='perfil'),
    url(r'^editar/(?P<id_pesquisador>[0-9]+)/$', editar_pesquisador, name='editar'),
    url(r'^me-editar/$', editar_meu_perfil, name='meeditar'),
   	url(r'^eu/$', meu_perfil, name='perfil'),
]
