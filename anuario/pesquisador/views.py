from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from .models import Perfil, Curso
from django.http import Http404
from django.contrib.auth.models import User
from django.shortcuts import get_list_or_404, get_object_or_404

from .forms import FormEditPerfil

# Create your views here.
def todos_pesquisadores(request):
	context = {}
	context['pesquisadores'] = Perfil.objects.all()
	return render (request, 'todos.html', context)

def meu_perfil(request):
	context = {}
	usuario = request.user
	pesquisador = Perfil.objects.get(usuario=usuario)

	context['pesquisador'] = pesquisador
	context['perfil'] = pesquisador.usuario

	return render(request, 'meu_perfil.html', context)

def editar_meu_perfil(request):
	
	usuario = User.objects.get(pk=request.user.id)
	pesquisador = Perfil.objects.get(usuario_id=usuario.id)
	print(pesquisador)
	context = {}

	if request.method == "POST":
		form = FormEditPerfil(request.POST or None, instance=pesquisador.curso)
		if form.is_valid():
			form.save()
	else:
		form = FormEditPerfil(instance=pesquisador)

	context['form'] = form
	return render(request, 'editar_minha.html', context)


def editar_pesquisador(request, id_pesquisador):
	
	pesquisador = Perfil.objects.get(pk=id_pesquisador)
	context = {}
	if request.method == "POST":
		form = FormEditPerfil(request.POST or None, instance=pesquisador.curso)
		form.curso = pesquisador.curso.nome_do_curso
		if form.is_valid():
			form.save()
	else:
		form = FormEditPerfil(instance=pesquisador)
	
	context['form'] = form
	
	return render(request, 'add_pesquisador.html', context)
