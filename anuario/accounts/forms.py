from django import forms
from anuario.pesquisador.models import Curso

from django.contrib.auth import (
    authenticate,
    get_user_model,
    login,
    logout
)

User = get_user_model()

class AccountLoginForm(forms.Form):
    username = forms.EmailField(widget=forms.TextInput(attrs={'class': 'input'}))
    password = forms.CharField(widget=forms.PasswordInput(attrs={'class':'input'}))

    def clean(self, *args, **kwargs):
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")

        # if not user:
        #     self.add_error('username', forms.ValidationError("Esse username não existe"))
        # elif not user.check_password(password):
        #     self.add_error('password', forms.ValidationError("A senha não confere"))
        # elif not user.is_active:
        #     raise forms.ValidationError("Esse usuário foi desativado")
        
        return super(AccountLoginForm, self).clean(*args, **kwargs)


class RegisterAccountForm(forms.ModelForm):
    attrs = {'class': 'input'} # Classe para se integrar com o framework Bulma.

    first_name   = forms.CharField(label='Qual seu nome?')
    username = forms.EmailField(label='Informe seu e-mail', widget=forms.EmailInput(attrs=attrs))

    opcoes_curso = (forms.ModelChoiceField(queryset=Curso.objects.all()).choices)
    curso = forms.CharField(label='Informe seu curso.', widget=forms.Select(choices=opcoes_curso))

    password = forms.CharField(label='Informe sua senha.', widget=forms.PasswordInput(attrs=attrs), required=True)
    password2 = forms.CharField(label='Repita sua senha.', widget=forms.PasswordInput(attrs=attrs), required=True)

    class Meta:
        model = User
        fields = [
            'first_name',
            'username',
            'curso',
            'password',
            'password2',
        ]

    def clean_username(self):
        username = self.cleaned_data.get('username')

        username_qs = User.objects.filter(username=username)
        if username_qs.exists():
            raise forms.ValidationError("Este E-mail já está registrado")

        return username


    def clean_curso(self):
        curso = self.cleaned_data.get('curso')
        print(curso)
        print(Curso.objects.get(id=curso).id)
        if (str(curso) != str(Curso.objects.get(id=curso).id)):
            raise forms.ValidationError('Escolha um curso!')
        else:
            return curso

    def clean_password(self):
        password = self.cleaned_data.get('password')

        if len(password) < 8:
            raise forms.ValidationError('Suas senhas deve conter ao menos 8 caracteres!')

        return password

    def clean_password2(self):
        password = self.cleaned_data.get('password')
        password2 = self.cleaned_data.get('password2')

        if password != password2:
            raise forms.ValidationError('Suas senhas são diferentes!')

        return password2

    def save(self, commit=True):
        user = super(RegisterAccountForm, self).save(commit=False)
        user.set_password(self.cleaned_data.get("password"))
        if commit:
            user.save()
        return user

    widgets = {
        'first_name': forms.TextInput(attrs={'placeholder': 'Seu nome completo'}),
        'nome_cientifico': forms.TextInput(attrs={'placeholder': 'Para referencias bibliográficas'}),
    }